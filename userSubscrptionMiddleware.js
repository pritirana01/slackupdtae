const supabase = require('./supaBaseClient');

async function checkSubscription(req, res, next) {
  const slackUserId = req.body.slackuserid;

  try {
    const { data: subscription, error } = await supabase
      .from('teammemberinfo')
      .select('is_subscribed')
      .eq('slackuserid', slackUserId)
      .single();

    if (error || !subscription.is_subscribed) {
      return res.status(403).json({ message: 'You are not subscribed to updates.' });
    }

    next();
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
}

module.exports = checkSubscription;
