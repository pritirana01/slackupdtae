const express = require('express');
const bodyParser = require('body-parser');
const { WebClient } = require('@slack/web-api');
const supabase = require('./supaBaseClient');
const cron = require('node-cron');
const axios = require('axios');
require('dotenv').config();

const app = express();
const PORT = process.env.PORT || 5000;

// Slack Web API client
const token = process.env.SLACK_TOKEN;
const slackClient = new WebClient(token);
console.log("__________",slackClient)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Middleware to extract slackuserid from request body if present
//it will pass controller to next funtionality
app.use((req, res, next) => {
  if (req.body.slackuserid) {
    req.slackuserid = req.body.slackuserid;
  }
  next();
});

// Serve the index.html file
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// API endpoint to fetch Slack user details
app.get('/api/slack/users', async (req, res) => {
  try {
    const channelID = process.env.SLACK_CHANNEL_ID;  //geting channel id

    const membersResult = await slackClient.conversations.members({ channel: channelID });
    //using sending a request to the slack client using built in funciton conversation.members function and fetch member id 

    const memberDetails = await Promise.all(
      membersResult.members.map(async (memberId) => {
        const userResult = await slackClient.users.info({ user: memberId });
        return {
          id: userResult.user.id,
          name: userResult.user.real_name,
        };
      })
    );
    res.json(memberDetails);
  } catch (error) {
    console.error('Error fetching Slack users:', error);
    res.status(500).json({ error: 'Failed to fetch Slack users' });
  }
  //getting details infromation about specfic users   
  //having  a list of slack id using .map function ..this function will go to the each elememt of list one by one and based on the user id it will fetch the user name  using built in function ussers.info
});


app.post('/api/teammembers', async (req, res) => {
  const { name, role } = req.body;
  const slackuserid = req.slackuserid;
  const teamMember = { slackuserid, name, role };

  try {
    const { data, error } = await supabase
      .from('projectDetails1')
      .insert([teamMember]);

    if (error) throw error;

    res.status(201).json(data[0]);
  } catch (err) {
    console.error('Error inserting team member:', err);
    res.status(400).json({ message: err.message });
  }
});

app.post('/api/tasks', async (req, res) => {
  const { taskName, taskDescription, timeDeadline } = req.body;
  const slackuserid = req.slackuserid;
  const task = { task: taskName, taskDescription, deadline: timeDeadline, slackuserid };

  try {
    const { data, error } = await supabase
      .from('projectDetails1')
      .insert([task]);

    if (error) throw error;

    res.status(201).json(data[0]);
  } catch (err) {
    console.error('Error inserting task:', err);
    res.status(400).json({ message: err.message });
  }
});

// Endpoint to enable notifications for a user



app.post('/api/enable_notifications', async (req, res) => {
  const { slackuserid, notification_time, notification_days } = req.body;

  if (!slackuserid) {
    return res.status(400).json({ message: 'slackuserid is required' });
  }

  try {
    console.log(`Enabling notifications for user: ${slackuserid}`);

    // Check if the user exists
    let { data: userCheck, error: userCheckError } = await supabase
      .from('user_notifications')
      .select('*')
      .eq('slackuserid', slackuserid);

    if (userCheckError) {
      console.error('Error fetching user:', userCheckError);
      return res.status(500).json({ message: 'Error fetching user', error: userCheckError.message });
    }

    // If user does not exist, insert the user
    if (userCheck.length === 0) {
      console.log('User not found, inserting new user');
      const { data: insertData, error: insertError } = await supabase
        .from('user_notifications')
        .insert([{ slackuserid, enable_notifications: true, notification_time, notification_days }]);

      if (insertError) {
        console.error('Error inserting user:', insertError);
        return res.status(500).json({ message: 'Error inserting user', error: insertError.message });
      }

      return res.status(200).json({ message: 'Notifications enabled for new user', data: insertData });
    }

    // Update the user's notification preference if they already exist
    const { data, error } = await supabase
      .from('user_notifications')
      .update({ enable_notifications: true, notification_time, notification_days })
      .eq('slackuserid', slackuserid);

    if (error) throw error;

    res.status(200).json({ message: 'Notifications enabled', data });
  } catch (err) {
    console.error('Error enabling notifications:', err);
    res.status(500).json({ message: err.message });
  }
});

// Endpoint to disable notifications for a user
app.post('/api/disable_notifications', async (req, res) => {
  const slackuserid = req.slackuserid;

  try {
    const { data, error } = await supabase
      .from('user_notifications')
      .update({ enable_notifications: false })
      .eq('slackuserid', slackuserid);

    if (error) throw error;

    res.status(200).json({ message: 'Notifications disabled', data });
  } catch (err) {
    console.error('Error disabling notifications:', err);
    res.status(400).json({ message: err.message });
  }
});

app.get('/api/tasks/:id', async (req, res) => {
  const { id } = req.params;

  try {
    const { data, error } = await supabase
      .from('projectDetails1')
      .select('*')
      .eq('id', id)
      .single();

    if (error) throw error;

    res.status(200).json(data);
  } catch (err) {
    console.error('Error fetching task:', err);
    res.status(400).json({ message: err.message });
  }
});

app.put('/api/tasks/:id', async (req, res) => {
  const { id } = req.params;
  const { taskName, taskDescription, timeDeadline } = req.body;
  const task = { task: taskName, taskDescription, deadline: timeDeadline };

  try {
    const { data, error } = await supabase
      .from('projectDetails1')
      .update(task)
      .eq('id', id);

    if (error) throw error;

    res.status(200).json(data[0]);
  } catch (err) {
    console.error('Error updating task:', err);
    res.status(400).json({ message: err.message });
  }
});

// Schedule notifications every 10 minutes
cron.schedule('*/1 * * * *', async () => {
  try {
    console.log('Cron job started');
    // Fetch users from the database who have not explicitly disabled notifications
    const { data: users, error } = await supabase
      .from('user_notifications')
      .select('*')
      .neq('enable_notifications', false);

    if (error) {
      console.error('Error fetching user preferences:', error);
      return;
    }

    if (users.length === 0) {
      console.log('No users found with enabled notifications.');
      return;
    }

    for (const user of users) {
      console.log(`Sending notification to user: ${user.slackuserid}`);
      await slackClient.chat.postMessage({
        channel: user.slackuserid, //target the user
        
        text: 'Reminder: Please update your daily progress.',
        blocks: [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: 'Reminder: Please update your daily progress.'
            }
          },
          {
            type: 'actions',
            elements: [
              {
                type: 'button',
                text: {
                  type: 'plain_text',
                  text: 'Update Work Details'
                },
                action_id: 'update_work_details'
              }
            ]
          }
        ]
      });
    }
    console.log('Notifications sent to enabled users.');
  } catch (error) {
    console.error('Error sending notifications:', error);
  }
}, {
  timezone: 'America/New_York'
});

app.post('/slack/interactivity', async (req, res) => {
  //when someone try to interact with the Bot it will send the request to the 
  //slack it 
  try {
    const payload = JSON.parse(req.body.payload);
    console.log('Received Slack payload:', payload);

    // Send immediate response to avoid timeout
    res.status(200).send('');

    if (payload.type === 'block_actions') {
      const action = payload.actions[0];
      const slackuserId = payload.user.id;

      if (action.action_id === 'disable_notifications') {
        // Update user notification preferences in the database
        await supabase
          .from('user_notifications')
          .update({ enable_notifications: false })
          .eq('slackuserid', slackuserId);

        await slackClient.chat.postMessage({
          channel: slackuserId,
          text: 'Notifications have been disabled for your account.'
        });
      } else if (action.action_id === 'enable_notifications') {
        // Update user notification preferences in the database
        await supabase
          .from('user_notifications')
          .update({ enable_notifications: true })
          .eq('slackuserid', slackuserId);

        await slackClient.chat.postMessage({
          channel: slackuserId,
          text: 'Notifications have been enabled for your account.'
        });
      } else if (action.action_id === 'update_work_details') {
        // Handle update work details
        const workDetailsModal = {
          type: 'modal',
          callback_id: 'update_workdetails_form',
          title: {
            type: 'plain_text',
            text: 'Update Work Details'
          },
          blocks: [
            {
              type: 'input',
              block_id: 'task_finished',
              label: {
                type: 'plain_text',
                text: 'What task did you finish today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'taskFinished'
              }
            },
            {
              type: 'input',
              block_id: 'business_impact',
              label: {
                type: 'plain_text',
                text: 'What business impact did you create today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'businessImpact'
              }
            },
            {
              type: 'input',
              block_id: 'challenges_faced',
              label: {
                type: 'plain_text',
                text: 'What challenges did you face today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'challengesFaced'
              }
            },
            {
              type: 'input',
              block_id: 'did_well',
              label: {
                type: 'plain_text',
                text: 'What did you do well today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'didWell'
              }
            },
            {
              type: 'input',
              block_id: 'did_not_well',
              label: {
                type: 'plain_text',
                text: 'What did you not do well today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'didNotWell'
              }
            },
            {
              type: 'input',
              block_id: 'feedback',
              label: {
                type: 'plain_text',
                text: 'Is there any feedback you want to give to anyone today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'feedback'
              }
            }
          ],
          submit: {
            type: 'plain_text',
            text: 'Submit'
          }
        };

        await slackClient.views.open({
          trigger_id: payload.trigger_id,
          view: workDetailsModal
        });

        return;
      } else if (action.action_id === 'edit_work_details') {
        const response = JSON.parse(action.value);
        const workDetailsModal = {
          type: 'modal',
          callback_id: 'edit_workdetails_form',
          title: {
            type: 'plain_text',
            text: 'Edit Work Details'
          },
          blocks: [
            {
              type: 'input',
              block_id: 'task_finished',
              label: {
                type: 'plain_text',
                text: 'What task did you finish today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'taskFinished',
                initial_value: response.task
              }
            },
            {
              type: 'input',
              block_id: 'business_impact',
              label: {
                type: 'plain_text',
                text: 'What business impact did you create today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'businessImpact',
                initial_value: response.taskdescription
              }
            },
            {
              type: 'input',
              block_id: 'challenges_faced',
              label: {
                type: 'plain_text',
                text: 'What challenges did you face today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'challengesFaced',
                initial_value: response.challengesfaced
              }
            },
            {
              type: 'input',
              block_id: 'did_well',
              label: {
                type: 'plain_text',
                text: 'What did you do well today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'didWell',
                initial_value: response.didwell
              }
            },
            {
              type: 'input',
              block_id: 'did_not_well',
              label: {
                type: 'plain_text',
                text: 'What did you not do well today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'didNotWell',
                initial_value: response.didnotwell
              }
            },
            {
              type: 'input',
              block_id: 'feedback',
              label: {
                type: 'plain_text',
                text: 'Is there any feedback you want to give to anyone today?'
              },
              element: {
                type: 'plain_text_input',
                action_id: 'feedback',
                initial_value: response.feedback
              }
            }
          ],
          submit: {
            type: 'plain_text',
            text: 'Submit'
          }
        };

        await slackClient.views.open({
          trigger_id: payload.trigger_id,
          view: workDetailsModal
        });

        return;
      } else if (action.action_id === 'customize_summary_day') {
        // Handle day selection for summary
        const selectedDay = action.selected_option.value;
        await supabase
          .from('user_notifications')
          .update({ summary_days: selectedDay })
          .eq('slackuserid', slackuserId);

        await slackClient.chat.postMessage({
          channel: slackuserId,
          text: `Your summary day has been set to ${selectedDay.charAt(0).toUpperCase() + selectedDay.slice(1)}.`
        });
      } 




      
      //custominzing the notification timing
      else if (action.action_id === 'timepicker') {
        const selectedTime = action.selected_option.value;

        try {
          const { error } = await supabase
            .from('user_notifications')
            .update({ notification_time: selectedTime })
            .eq('slackuserid', slackuserId);

          if (error) {
            throw new Error(`Database update failed: ${error.message}`);
          }

          await slackClient.chat.postMessage({
            channel: slackuserId,
            text: `Your notification time has been set to ${selectedTime}.`
          });
        } catch (err) {
          console.error(`Error updating notification time or sending message: ${err.message}`);
        }
      } 
      
      
      
      
      else if (action.action_id === 'generate_summary') {
        console.log("***************************")
        const selectedDay = payload.view.state.values.date_range_end_input.end_date.selected_date;
        console.log(selectedDay);
        if (payload) {
          const startDate = payload.view.state.values.date_range_start_input.start_date.selected_date;
          console.log("date",startDate);
          const endDate = payload.view.state.values.date_range_end_input.end_date.selected_date;
          console.log("date***********",endDate);
          const { data: userResponses, error } = await supabase
            .from('projectdetails1')
            .select('*')
            .eq('slackuserid', slackuserId)
            .gte('created_at', startDate)
            .lte('created_at', endDate);

          if (error) {
            console.error('Error fetching responses:', error);
            throw error;
          }

          const formattedData = formatDataForOpenAI(userResponses);
          const summary = await generateSummary(formattedData);

          await slackClient.chat.postMessage({
            channel: slackuserId,
            text: `Summary for the date range ${startDate} to ${endDate}:\n${summary}`
          });
        } else {
          console.error('Error: Missing required data in the payload');
          await slackClient.chat.postMessage({
            channel: slackuserId,
            text: 'Error: Missing required data to generate summary. Please try again.'
          });
        }
      }
    } else if (payload.type === 'view_submission') {
      const slackuserId = payload.user.id;

      if (payload.view.callback_id === 'update_workdetails_form' || payload.view.callback_id === 'edit_workdetails_form') {
        // Handle form submission
        const taskFinished = payload.view.state.values.task_finished.taskFinished.value;
        const businessImpact = payload.view.state.values.business_impact.businessImpact.value;
        const challengesfaced = payload.view.state.values.challenges_faced.challengesFaced.value;
        const didwell = payload.view.state.values.did_well.didWell.value;
        const didnotwell = payload.view.state.values.did_not_well.didNotWell.value;
        const feedback = payload.view.state.values.feedback.feedback.value;

        const userResult = await slackClient.users.info({ user: slackuserId });
        const name = userResult.user.real_name;

        const taskData = {
          task: taskFinished,
          taskdescription: businessImpact,
          challengesfaced,
          didwell,
          didnotwell,
          feedback,
          slackuserid: slackuserId,
          name
        };
        console.log('Task data to be inserted:', taskData);

        if (payload.view.callback_id === 'edit_workdetails_form') {
          const { data: recentTask, error: fetchError } = await supabase
            .from('projectdetails1')
            .select('*')
            .eq('slackuserid', slackuserId)
            .order('created_at', { ascending: false })
            .limit(1);

          if (fetchError) {
            console.error('Error fetching recent task:', fetchError);
            throw fetchError;
          }

          if (!recentTask || recentTask.length === 0) {
            return res.status(404).json({ message: 'No recent task found for user' });
          }

          const taskId = recentTask[0].id;
          const { data, error } = await supabase
            .from('projectdetails1')
            .update(taskData)
            .eq('id', taskId)
            .eq('slackuserid', slackuserId);

          if (error) {
            console.error('Error updating task:', error);
            throw error;
          }

          await sendUpdateToSlack({
            ...taskData,
            id: taskId
          });
        } else {
          const { data, error } = await supabase
            .from('projectdetails1')
            .insert([taskData]);

          if (error) {
            console.error('Error inserting data into Supabase:', error);
            throw error;
          }

          await sendUpdateToSlack(taskData);
        }
      } else if (payload.view.callback_id === 'customize_notifications_form') {
        const notificationTime = payload.view.state.values.time_input.timepicker.selected_time;
        console.log("*************************************************",notificationTime)
        const notificationDays = payload.view.state.values.days_input.days_select.selected_options.map(option => option.value).join(',');
        const summaryDays = payload.view.state.values.summary_day_input.summary_day_select.selected_options.map(option => option.value).join(',');
          
        // console.log('Notification Time:', notificationTime);
        // console.log('Notification Days:', notificationDays);
        // console.log('Summary Days:', summaryDays);



        await supabase
          .from('user_notifications')
          .upsert({ slackuserid: slackuserId, notification_time: notificationTime, notification_days: notificationDays, summary_days: summaryDays }, { onConflict: 'slackuserid' });
          

        await slackClient.chat.postMessage({
          channel: slackuserId,
          text: 'Your notification preferences have been updated!'
        });
      }
    }
  } catch (error) {
    console.error('Error handling interactive payload:', error);
    return res.status(500).send('Error handling interactive payload');
  }
});

app.post('/slack/events', async (req, res) => {
  const { type, event, challenge } = req.body;
  console.log("testing the user id--------->",event.user)

  if (type === 'url_verification') {
    return res.status(200).send({ challenge: req.body.challenge });
  } else if (type === 'event_callback' && event.type === 'app_home_opened') {
    try {
      // Fetch user preferences from the database
      const { data: userPreferences, error } = await supabase
        .from('user_notifications')
        .select('*')
        .eq('slackuserid', event.user);

      if (error) throw error;

      const userPreference = userPreferences[0] || {};

      const initialNotificationOptions = userPreference.notification_days
        ? userPreference.notification_days.split(',').map(day => ({
            text: {
              type: 'plain_text',
              text: day.charAt(0).toUpperCase() + day.slice(1)
            },
            value: day
          }))
        : [];

      const initialSummaryOptions = userPreference.summary_days
        ? userPreference.summary_days.split(',').map(day => ({
            text: {
              type: 'plain_text',
              text: day.charAt(0).toUpperCase() + day.slice(1)
            },
            value: day
          }))
        : [];

      await slackClient.views.publish({
        user_id: event.user,
        view: {
          type: 'home',
          blocks: [
            {
              type: 'header',
              text: {
                type: 'plain_text',
                text: 'Welcome to Your Home Page! :house_with_garden:',
                emoji: true
              }
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: '*Manage Your Notifications* :bell:'
              }
            },
            {
              type: 'actions',
              elements: [
                {
                  type: 'button',
                  text: {
                    type: 'plain_text',
                    text: 'Enable Notifications :white_check_mark:',
                    emoji: true
                  },
                  value: 'enable_notifications',
                  action_id: 'enable_notifications'
                },
                {
                  type: 'button',
                  text: {
                    type: 'plain_text',
                    text: 'Disable Notifications :x:',
                    emoji: true
                  },
                  value: 'disable_notifications',
                  action_id: 'disable_notifications'
                }
              ]
            },
            {
              type: 'divider'
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: '*Notification Customization* :clock1:'
              }
            },
            {
              type: 'input',
              block_id: 'time_input',
              label: {
                type: 'plain_text',
                text: 'Set Notification Time :timer_clock:',
                emoji: true
              },
              element: {
                type: 'timepicker',
                action_id: 'timepicker',
                initial_time: userPreference.notification_time || '09:00',
  
                placeholder: {
                  type: 'plain_text',
                  text: 'Select time',
                  emoji: true
                }
              }
            },
          
            {
              type: 'input',
              block_id: 'days_input',
              label: {
                type: 'plain_text',
                text: 'Select Days for Notifications :calendar:',
                emoji: true
              },
              element: {
                type: 'multi_static_select',
                action_id: 'days_select',
                initial_options: initialNotificationOptions.length > 0 ? initialNotificationOptions : undefined,
                placeholder: {
                  type: 'plain_text',
                  text: 'Select days',
                  emoji: true
                },
                options: [
                  {
                    text: {
                      type: 'plain_text',
                      text: 'Monday'
                    },
                    value: 'monday'
                  },
                  {
                    text: {
                      type: 'plain_text',
                      text: 'Tuesday'
                    },
                    value: 'tuesday'
                  },
                  {
                    text: {
                      type: 'plain_text',
                      text: 'Wednesday'
                    },
                    value: 'wednesday'
                  },
                  {
                    text: {
                      type: 'plain_text',
                      text: 'Thursday'
                    },
                    value: 'thursday'
                  },
                  {
                    text: {
                      type: 'plain_text',
                      text: 'Friday'
                    },
                    value: 'friday'
                  },
                  {
                    text: {
                      type: 'plain_text',
                      text: 'Saturday'
                    },
                    value: 'saturday'
                  },
                  {
                    text: {
                      type: 'plain_text',
                      text: 'Sunday'
                    },
                    value: 'sunday'
                  }
                ]
              }
            },
            {
              type: 'divider'
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: '*Summary Configuration* :memo:'
              }
            },
            {
              type: 'actions',
              block_id: 'summary_days_input',
              elements: [
                {
                  type: 'static_select',
                  action_id: 'customize_summary_day',
                  placeholder: {
                    type: 'plain_text',
                    text: 'Select a day'
                  },
                  options: [
                    {
                      text: {
                        type: 'plain_text',
                        text: 'Monday'
                      },
                      value: 'monday'
                    },
                    {
                      text: {
                        type: 'plain_text',
                        text: 'Tuesday'
                      },
                      value: 'tuesday'
                    },
                    {
                      text: {
                        type: 'plain_text',
                        text: 'Wednesday'
                      },
                      value: 'wednesday'
                    },
                    {
                      text: {
                        type: 'plain_text',
                        text: 'Thursday'
                      },
                      value: 'thursday'
                    },
                    {
                      text: {
                        type: 'plain_text',
                        text: 'Friday'
                      },
                      value: 'friday'
                    },
                    {
                      text: {
                        type: 'plain_text',
                        text: 'Saturday'
                      },
                      value: 'saturday'
                    },
                    {
                      text: {
                        type: 'plain_text',
                        text: 'Sunday'
                      },
                      value: 'sunday'
                    }
                  ],
                  initial_option: initialSummaryOptions.length > 0 ? initialSummaryOptions[0] : undefined
                }
              ]
            },
            {
              type: 'divider'
            },
            {
              type: 'input',
              block_id: 'date_range_start_input',
              element: {
                type: 'datepicker',
                action_id: 'start_date',
                placeholder: {
                  type: 'plain_text',
                  text: 'Select a start date',
                  emoji: true
                }
              },
              label: {
                type: 'plain_text',
                text: 'Start Date',
                emoji: true
              }
            },
            {
              type: 'input',
              block_id: 'date_range_end_input',
              element: {
                type: 'datepicker',
                action_id: 'end_date',
                placeholder: {
                  type: 'plain_text',
                  text: 'Select an end date',
                  emoji: true
                }
              },
              label: {
                type: 'plain_text',
                text: 'End Date',
                emoji: true
              }
            },
            {
              type: 'actions',
              elements: [
                {
                  type: 'button',
                  text: {
                    type: 'plain_text',
                    text: 'Generate Summary'
                  },
                  value: 'generate_summary',
                  action_id: 'generate_summary'
                }
              ]
            }
          ]
        }
      });
    } catch (error) {
      console.error('Error publishing home tab:', error);
      return res.status(500).send('Error publishing home tab');
    }
  }
  res.sendStatus(200);
});

// Format data for Slack messages
const formatDataForSlackMessages = (responses) => {
  const currentDate = new Date().toLocaleDateString('en-US', {
    weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'
  });
  const workDetailsEmoji = ':memo:';

  return responses.map(response => ([
    {
      type: 'header',
      text: {
        type: 'plain_text',
        text: `${workDetailsEmoji} Work Details for ${currentDate}`
      }
    },
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `:white_check_mark: *Task Finished:* \n${response.task}\n\n:bulb: *Business Impact:* \n${response.taskdescription}\n\n:warning: *Challenges Faced:* \n${response.challengesfaced}\n\n:star: *What Did Well:* \n${response.didwell}\n\n:x: *What Did Not Do Well:* \n${response.didnotwell}\n\n:speech_balloon: *Feedback:* \n${response.feedback}`
      }
    },
    {
      type: 'actions',
      elements: [
        {
          type: 'button',
          text: {
            type: 'plain_text',
            text: 'Edit'
          },
          value: JSON.stringify(response),
          action_id: 'edit_work_details'
        }
      ]
    }
  ])).flat();
}

// Send individual updates as messages to Slack
async function sendUpdateToSlack(response) {
  console.log("response", response);
  const slackuserid = response.slackuserid;
  console.log("slackuserid", slackuserid);
  const formattedData = formatDataForSlackMessages([response]).flat();
  const blocks = [
    {
      type: 'header',
      text: {
        type: 'plain_text',
        text: `Update from ${response.name}`
      }
    },
    ...formattedData
  ];

  await slackClient.chat.postMessage({
    channel: slackuserid,
    blocks
  });
}

// Update API
app.post('/api/update_recent_task', async (req, res) => {
  const { task, taskdescription, slackuserid, challengesfaced, didwell } = req.body;

  try {
    // Fetch the most recent task entry for the user
    const { data: recentTask, error: fetchError } = await supabase
      .from('projectdetails1')
      .select('*')
      .eq('slackuserid', slackuserid)
      .order('created_at', { ascending: false })
      .limit(1);

    // Log the results for debugging
    console.log('Fetched recent task:', recentTask);

    if (fetchError) {
      console.error('Error fetching recent task:', fetchError);
      return res.status(500).json({ message: fetchError.message });
    }

    if (!recentTask || recentTask.length === 0) {
      return res.status(404).json({ message: 'No recent task found for user' });
    }

    const taskId = recentTask[0].id;
    console.log("id", taskId);

    // Update the most recent task entry
    const { data, error } = await supabase
      .from('projectdetails1')
      .update({
        id: taskId,
        task: task,
        taskdescription,
        slackuserid: slackuserid,
        name: recentTask[0].name,
        challengesfaced: challengesfaced || recentTask[0].challengesfaced,
        didwell: didwell || recentTask[0].didwell,
        feedback: recentTask[0].feedback,
      })
      .eq('id', taskId)
      .eq('slackuserid', slackuserid);

    if (error) {
      console.error('Error updating task:', error);
      throw error;
    }

    await sendUpdateToSlack({
      id: taskId,
      task: task,
      taskdescription,
      slackuserid: slackuserid,
      name: recentTask[0].name,
      challengesfaced: challengesfaced || recentTask[0].challengesfaced,
      didwell: didwell || recentTask[0].didwell,
      feedback: recentTask[0].feedback,
    });

    console.log("updated ", data);
    res.status(200).json(data);

  } catch (err) {
    console.error('Error updating recent task:', err);
    res.status(400).json({ message: err.message });
  }
});
async function fetchResponses(startDate, endDate) {
  const { data, error } = await supabase
    .from('projectdetails1')
    .select('*')
    .gte('created_at', startDate)
    .lte('created_at', endDate);

  if (error) {
    console.error('Error fetching data:', error);
    return [];
  }

  return data;
}

// Format data for OpenAI API
function formatDataForOpenAI(responses) {
  return responses.map(response =>
    `Tasks finished today: ${response.task}
    Wins/business impact: ${response.taskdescription}
    Challenges faced: ${response.challengesfaced}
    What did well: ${response.didwell}
    What did not do well: ${response.didnotwell}
    Feedback: ${response.feedback}`
  ).join('\n\n');
}

// Function to generate summary
async function generateSummary(text) {
  const prompt = `
Summarize the following daily reports in exactly 6 lines, with each line starting with an emoji and in the second line, there will be one line with text "Here is your summary". This line should start with a different emoji. Generate big lines.

Reports:
${text}

Summary:
`;

  const response = await axios.post('https://api.openai.com/v1/chat/completions', {
    model: 'gpt-3.5-turbo',
    messages: [
      { role: 'system', content: 'You are a helpful assistant.' },
      { role: 'user', content: prompt }
    ],
    max_tokens: 200,
  }, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${process.env.OPENAI_API_KEY}`
    }
  });

  let summary = response.data.choices[0].message.content.trim();

  // Format the heading to be bold with emojis before and after
  summary = summary.replace('Summary:', `:large_blue_diamond: Here is Your Summary :large_blue_diamond:`);

  // Ensure each line starts with an emoji
  summary = summary.split('\n').map(paragraph => ` ${paragraph}`).join('\n\n');

  return summary;
}

// Summarize daily reports and store in the database
async function summarizeDailyReports() {
  const startDate = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000).toISOString().split('T')[0];
  const endDate = new Date().toISOString().split('T')[0];
  const responses = await fetchResponses(startDate, endDate);
  const summaries = {};

  const groupedResponses = responses.reduce((acc, response) => {
    if (!acc[response.slackuserid]) {
      acc[response.slackuserid] = [];
    }
    acc[response.slackuserid].push(response);
    return acc;
  }, {});

  for (const [slackuserid, userResponses] of Object.entries(groupedResponses)) {
    const formattedData = formatDataForOpenAI(userResponses);
    const summary = await generateSummary(formattedData);
    summaries[slackuserid] = summary;
    console.log(`Summary for user ${slackuserid}:`, summary);

    // Fetch the user's name using the Slack user ID
    const userResult = await slackClient.users.info({ user: slackuserid });
    const name = userResult.user.real_name;

    // Insert summary into the database
    const { data, error } = await supabase
      .from('summaries')
      .insert([{ slackuserid, name, summary }]);
    if (error) {
      console.error('Error inserting summary into Supabase:', error);
      throw error;
    }
  }

  await sendSummariesToUsers(summaries);

  return summaries;
}

// Send summaries to users
async function sendSummariesToUsers(summaries) {
  for (const slackuserid in summaries) {
    const { data: userPreferences, error } = await supabase
      .from('user_notifications')
      .select('summary_days')
      .eq('slackuserid', slackuserid)
      .single();

    if (error) {
      console.error(`Error fetching preferences for user ${slackuserid}:`, error);
      continue;
    }

    const summaryDays = userPreferences.summary_days ? userPreferences.summary_days.split(',') : ['monday'];
    const currentDay = new Date().toLocaleString('en-us', { weekday: 'long' }).toLowerCase();

    if (summaryDays.includes(currentDay)) {
      await sendSummaryToSlack(slackuserid, summaries[slackuserid]);
    }
  }
}

async function sendSummaryToSlack(slackuserid, summary) {
  try {
    await slackClient.chat.postMessage({
      channel: slackuserid,
      text: `${summary}`
    });
  } catch (error) {
    console.error(`Error sending summary to user ${slackuserid}:`, error);
  }
}

// Schedule to collect daily reports every day
cron.schedule('0 23 * * *', async () => {
  try {
    await summarizeDailyReports();
  } catch (error) {
    console.error('Error in summarizeDailyReports:', error);
  }
}, {
  timezone: 'America/New_York'
});

// Schedule to send summary every 7 days
cron.schedule('0 0 * * 7', async () => {
  try {
    await summarizeDailyReports();
  } catch (error) {
    console.error('Error in summarizeDailyReports:', error);
  }
}, {
  timezone: 'America/New_York'
});
// app.get('/oauth/callback', async (req, res) => {
//   const code = req.query.code;
//   console.log('Received code:', code);

//   if (!code) {
//     return res.status(400).send('No code provided.');
//   }

//   try {
//     const response = await axios.post('https://slack.com/api/oauth.v2.access', null, {
//       params: {
//         client_id: process.env.SLACK_CLIENT_ID,
//         client_secret: process.env.SLACK_CLIENT_SECRET,
//         code: code,
//         redirect_uri: 'https://slackupdate-mros.onrender.com/oauth/callback' 
//       },
//       headers: {
//         'Content-Type': 'application/x-www-form-urlencoded'
//       }
//     });
//        console.log("testing req-------->",response)
//     const data = response.data;
//     console.log("Slack API response:", data);

//     if (!data.ok) {
//       return res.status(400).send(`Error: ${data.error}`);
//     }

//     await saveTokenToDatabase(data);

//     // Redirect to Slack URL after auth
//     res.redirect(`https://slack.com/app_redirect?app=${process.env.SLACK_APP_ID}`);
//   } catch (error) {
//     console.error('Error during OAuth callback:', error);
//     res.status(500).send('Internal Server Error');
//   }
// });

// // Function to save token to database 
// async function saveTokenToDatabase(data) {
//   // Save the data.access_token and other required information
//   console.log('OAuth data:', data);

//   // Inserting in Supabase
//   const { error } = await supabase
//     .from('tokens')
//     .insert([{ access_token: data.access_token, team_id: data.team.id, bot_user_id: data.bot_user_id }]);
  
//   if (error) {
//     console.error('Error saving token to database:', error);
//     throw error;
//   }
// }
app.get('/oauth/callback', async (req, res) => {
  const code = req.query.code;
  console.log('Received code:', code);

  if (!code) {
    return res.status(400).send('No code provided.');
  }

  try {
    // Exchange the code for an access token
    const response = await axios.post('https://slack.com/api/oauth.v2.access', null, {
      params: {
        client_id: process.env.SLACK_CLIENT_ID,
        client_secret: process.env.SLACK_CLIENT_SECRET,
        code: code,
        redirect_uri: 'https://slackupdate-mros.onrender.com/oauth/callback'
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });

    const data = response.data;
    console.log("Slack API response:", data);

    if (!data.ok) {
      return res.status(400).send(`Error: ${data.error}`);
    }

    // Save the token to the database
    await saveTokenToDatabase(data);

    // Set the URL for interactive components
    const setInteractiveUrlResponse = await axios.post('https://slack.com/api/apps.settings.interactive', {
      client_id: process.env.SLACK_CLIENT_ID,
      client_secret: process.env.SLACK_CLIENT_SECRET,
      token: data.access_token,  // Use the access token obtained from OAuth
      request_url: 'https://slackupdate-mros.onrender.com/slack/interactivity'
    });

    if (!setInteractiveUrlResponse.data.ok) {
      return res.status(400).send(`Error setting interactive URL: ${setInteractiveUrlResponse.data.error}`);
    }

    console.log("Interactive URL set:", setInteractiveUrlResponse.data);

    // Set the URL for event subscriptions
    const setEventUrlResponse = await axios.post('https://slack.com/api/apps.event.subscriptions.create', {
      token: data.access_token,  // Use the access token obtained from OAuth
      event_request_url: 'https://slackupdate-mros.onrender.com/slack/events',
      events: ["message.channels", "reaction_added"]
    });

    if (!setEventUrlResponse.data.ok) {
      return res.status(400).send(`Error setting event URL: ${setEventUrlResponse.data.error}`);
    }

    console.log("Event URL set:", setEventUrlResponse.data);

    // Redirect to Slack URL after setting up interactive components and events
    res.redirect(`https://slack.com/app_redirect?app=${process.env.SLACK_APP_ID}`);
  } catch (error) {
    console.error('Error during OAuth callback:', error);
    res.status(500).send('Internal Server Error');
  }
});

app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`);
});
